"use client";

import { ImageCard } from "@/components/ImageCard";
import { Template } from "@/components/Template";
import Image from "@/resources/image/image.resorce";
import { useImageService } from "@/resources/image/image.service";
import { useState } from "react";

export default function PageGallery() {
   const useService = useImageService()
   const [images, setImages] = useState<Image[]>([]);

   async function searchImages() {
      const result = await useService.search();
      setImages(result);
      console.log(result, 'aqui')
   }


   return (
      <Template>
         <button className="bg-gray-500" onClick={searchImages}>Clique para mudar</button>
         <section className="grid grid-cols-4 gap-8">
            <ImageCard name="teste" dataUpload="teste" size="" />
            <ImageCard />
         </section>
      </Template>
   )
}

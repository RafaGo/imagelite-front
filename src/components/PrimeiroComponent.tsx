'use client'

interface PrimeiroComponentProps {
   mensagem: string;
}


export const PrimeiroComponent: React.FC<PrimeiroComponentProps> = (props: PrimeiroComponentProps) => {

   function handleClick() {
      console.log("Cliquei aqui")
   }

   return (
      <div>
         {props.mensagem}

         <button onClick={handleClick}>Cliquei aqui</button>
      </div>
   )
}

export const ArrowFunction = () => {
   return (
      <h2>Arrow Function</h2>
   )
}
